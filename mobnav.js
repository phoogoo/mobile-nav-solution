/* =============================================================================
    Variable Settings
   ========================================================================== */
    var wp_menu_to_pull_from = '#nav',
        mobnav_toggle_parent = '#header';

/* =============================================================================
    Increased Responsiveness for devices that support touch
   ========================================================================== */
    var touchClick = 'click',
        mobnavDone = false;

    /* IF USING MODERNIZR: Disable delay on mobile for click events */
    if ( $('html').hasClass('touch') && $(window).width() <= 769 ) {
        touchClick = 'touchend';
    }

/* ==================================================
    Window-size Dependant Functions
   =============================================== */
    function getWindowSize() {
        var w = $(window).width(), h =  $(window).height();
        return { width : w, height : h };
    }

    function resized(){
        winsize = getWindowSize();

        /* Mobile Nav */
        if ( winsize.width <= 769 )
        {
            if ( !mobnavDone )
            {
                create_mobile_nav();  
                mobnavDone = true;
            }
        }
    }

/* ==================================================
    Mobile Nav Creation
   =============================================== */
    function create_mobile_nav() 
    {
        $(mobnav_toggle_parent).append('<a id="mobile-nav-toggle"><span>Menu</span></a>');
        $('body').append('<div id="mobile-nav"><a id="mobile-nav-close"><span>Close Menu</span></a><ul></ul></div>');

        $(wp_menu_to_pull_from).find('ul:not(.sub-menu)').each(function(){
            $('#mobile-nav ul').append($(this).html());
        });
    }
    
/* ==================================================
    Bind mobile nav toggle
   =============================================== */
    $(document).on(touchClick, '#mobile-nav-toggle, #mobile-nav-close', function(){
        $('#mobile-nav').toggleClass('on');
    });

/* ==================================================
    Establish debouncedresize https://github.com/louisremi/jquery-smartresize
   =============================================== */
    (function(d){var b=d.event,a,c;a=b.special.debouncedresize={setup:function(){d(this).on("resize",a.handler)},teardown:function(){d(this).off("resize",a.handler)},handler:function(i,e){var h=this,g=arguments,f=function(){i.type="debouncedresize";b.dispatch.apply(h,g)};if(c){clearTimeout(c)}e?f():c=setTimeout(f,a.threshold)},threshold:150}})(jQuery);

/* ==================================================
    Run resize function on debouncedresize
   =============================================== */
    $(window).on("debouncedresize", function( event ) {
        resized();
    });    

/* ==================================================
    Enact after page ready
   =============================================== */
jQuery(function($){

    resized();

});